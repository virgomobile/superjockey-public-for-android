package com.testjockey.apps.demo.superjockey.scenes;

import hu.virgo.testjockeysdk.core.TestJockey;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.util.adt.color.Color;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.testjockey.apps.demo.superjockey.Constants;
import com.testjockey.apps.demo.superjockey.gameelements.LevelObstacle;
import com.testjockey.apps.demo.superjockey.helpers.ObstacleFactory;

enum Level {
	FIRST,
	SECOND,
	THIRD,
}

public class LevelScene extends BaseScene {
	
	private static final int LVL_2_SCORE = 50;
	private static final int LVL_3_SCORE = 100;
	
	private static Level currentLevel = Level.FIRST;
	
	public LevelScene() {
		super();
	}
	
	public LevelScene(int lives, float currentSpeed, int score, int starScore, State currentState, int heroYPos, float yImpulse) {
		super(lives, currentSpeed, score, starScore, currentState, heroYPos, yImpulse);
		setCurrentLevel();
	}
	
	@Override
	protected Sprite createBg(float posX) {
		Sprite resultBg = null;
		switch (currentLevel) {
		case FIRST:
			resultBg = new Sprite(posX, 0, resourceManager.lvl1BgRegion, vbom);
			break;
		case SECOND:
			resultBg = new Sprite(posX, 0, resourceManager.lvl2BgRegion, vbom);
			break;
		case THIRD:
			resultBg = new Sprite(posX, 0, resourceManager.lvl3BgRegion, vbom);
			break;
		}
		
		resultBg.setAnchorCenter(0, 0);
		resultBg.setZIndex(-100);
		attachChild(resultBg);
		
		resultBg.setCullingEnabled(true);
		
		return resultBg;
	}

	protected Rectangle createTop(float posX) {
		Rectangle resultRectangle = new Rectangle(
				posX, 
				resourceManager.camera.getHeight(), 
				resourceManager.camera.getWidth(), 
				1, 
				vbom);
		resultRectangle.setColor(Color.BLACK);
		resultRectangle.setAlpha(1f);
		resultRectangle.setZIndex(1000);
		attachChild(resultRectangle);
		
		Body resultTopBody = PhysicsFactory.createBoxBody(physics, resultRectangle, BodyType.StaticBody, Constants.FIXTURE_TOP);
		resultTopBody.setUserData(Constants.BODY_TOP);
		return resultRectangle;
	}
	
	protected Sprite createBottom(float posX, float posY) {
		Sprite resultBottom = null;
		switch (currentLevel) {
			case FIRST:
				resultBottom = new Sprite(posX, posY, resourceManager.lvl1BottomRegion, vbom);
				break;
			case SECOND:
				resultBottom = new Sprite(posX, posY, resourceManager.lvl2BottomRegion, vbom);
				break;
			case THIRD:
				resultBottom = new Sprite(posX, posY, resourceManager.lvl3BottomRegion, vbom);
				break;
		}
		resultBottom.setAnchorCenter(0, 0);
		resultBottom.setZIndex(10);
		attachChild(resultBottom);
		
		PhysicsFactory.createBoxBody(physics, resultBottom.getX(), resultBottom.getY(), resultBottom.getWidth(), resultBottom.getHeight() / 1.5f, BodyType.StaticBody, Constants.FIXTURE_LEVEL)
			.setUserData(Constants.BODY_OBSTACLE);
		
		resultBottom.setCullingEnabled(true);
		
		return resultBottom;
	}

	protected void createStar(float posX, float posY) {
		createStar(posX, posY, resourceManager.starRegion);
	}
	
	protected void repeatLvlBackground() {
		if (!backgrounds.isEmpty()) {
			Sprite bgFirst = backgrounds.getFirst();
			Sprite bgSecond = backgrounds.get(1);
			if (bgFirst.getX() + bgFirst.getWidth() < resourceManager.camera.getXMin()) {
				backgrounds.remove();
				setCurrentLevel();
				backgrounds.add(createBg(bgSecond.getX() + bgSecond.getWidth()));
				bgFirst = null;
			}
		}
	}

	protected void repeatBottom() {
		if (!bottoms.isEmpty()) {
			Sprite bottomFirst = bottoms.getFirst();
			Sprite bottomSecond = bottoms.get(1);
			if (bottomFirst.getX() + bottomFirst.getWidth() < resourceManager.camera.getXMin()) {
				bottoms.remove();
				setCurrentLevel();
				bottoms.add(createBottom(bottomSecond.getX() + bottomSecond.getWidth(), Constants.BOTTOM_Y_POSITION));
				bottomFirst = null;
			}
		}
	}

	private void setCurrentLevel() {
		if (getOverallScore() < LVL_2_SCORE) {
			currentLevel = Level.FIRST;
		} else if (getOverallScore() >= LVL_2_SCORE && getOverallScore() < LVL_3_SCORE) {
			if (currentLevel != Level.SECOND) {
				currentLevel = Level.SECOND;
				TestJockey.pinFlag(TestJockey.Flag.builder(Constants.TJ_FLAG_LEVEL2).requestTelemetry(true).build());				
			}
		} else {
			if (currentLevel != Level.THIRD) {
				currentLevel = Level.THIRD;
				TestJockey.pinFlag(TestJockey.Flag.builder(Constants.TJ_FLAG_LEVEL3).requestTelemetry(true).build());				
			}
		}
	}
	
	@Override
	protected void increaseStarScore() {
		starScore += difficultyMultiplier;
		refreshScoreText();
		sendScoreFlags();
	}
	
	protected void addObstacle() {
		ObstacleFactory of = ObstacleFactory.getInstance();
		LevelObstacle obstacle = of.nextLevelObstacle(score / 2);
		obstacles.add(obstacle);
		float posY = (float) Math.random() * 350 + 250;
		createStar(obstacle.getX() + obstacle.getWidth() + of.getObstacleDistance() / 2, posY);
		
		if (!obstacle.hasParent()) {
			attachChild(obstacle);
		}
	}
	
	@Override
	public void beginContact(Contact contact) {
		if (Constants.BODY_TOP.equals(contact.getFixtureA().getBody().getUserData()) ||
				Constants.BODY_TOP.equals(contact.getFixtureB().getBody().getUserData())) {
			SpaceScene spaceScene = new SpaceScene(lives.size(), heroXSpeed, score, starScore, state, 50, 200);
			resourceManager.engine.setScene(spaceScene);
		}
		super.beginContact(contact);
	}
	
	public void endContact(Contact contact) {
		if (state == State.PLAY) {
			if (Constants.BODY_SENSOR.equals(contact.getFixtureA().getBody().getUserData()) ||
					Constants.BODY_SENSOR.equals(contact.getFixtureB().getBody().getUserData())) {
				score += difficultyMultiplier;
				sensorLeaved = true;
				refreshScoreText();
				sendScoreFlags();
			}			
		}
		super.endContact(contact);		
	}

	@Override
	protected void startNewGame() {
		setCurrentLevel();
	}
	
}
