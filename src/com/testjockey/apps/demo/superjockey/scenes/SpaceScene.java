package com.testjockey.apps.demo.superjockey.scenes;

import hu.virgo.testjockeysdk.core.TestJockey;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.util.adt.color.Color;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.testjockey.apps.demo.superjockey.Constants;
import com.testjockey.apps.demo.superjockey.gameelements.SpaceObstacle;
import com.testjockey.apps.demo.superjockey.helpers.ObstacleFactory;

public class SpaceScene extends BaseScene {
	
	public SpaceScene() {
		super();
	}
	
	public SpaceScene(int lives, float currentSpeed, int score, int starScore, State currentState, int heroYPos, float yImpulse) {
		super(lives, currentSpeed, score, starScore, currentState, heroYPos, yImpulse);
		TestJockey.pinFlag(TestJockey.Flag.builder(Constants.TJ_FLAG_LEVEL_SPACE).requestTelemetry(true).build());
	}
	
	protected Sprite createBg(float posX){
		Sprite resultBg = new Sprite(posX, 0, resourceManager.lvlSpaceRegion, vbom);
		resultBg.setAnchorCenter(0, 0);
		resultBg.setZIndex(-100);
		attachChild(resultBg);
		
		resultBg.setCullingEnabled(true);
		
		return resultBg;
	}
	
	protected Rectangle createTop(float posX) {
		Rectangle resultRectangle = new Rectangle(
				posX, 
				resourceManager.camera.getHeight(), 
				resourceManager.camera.getWidth(), 
				1, 
				vbom);
		resultRectangle.setColor(Color.BLACK);
		resultRectangle.setAlpha(1f);
		resultRectangle.setZIndex(1000);
		attachChild(resultRectangle);
		
		Body resultTopBody = PhysicsFactory.createBoxBody(physics, resultRectangle, BodyType.StaticBody, Constants.FIXTURE_TOP);
		resultTopBody.setUserData(Constants.BODY_TOP);
		return resultRectangle;
	}
	
	protected Sprite createBottom(float posX, float posY) {
		Sprite resultBottom = new Sprite(
				posX, 
				posY, 
				resourceManager.emptyGroundRegion, 
				vbom);
		resultBottom.setAnchorCenter(0, 0);
		resultBottom.setZIndex(10);
		attachChild(resultBottom);
		
		PhysicsFactory.createBoxBody(physics, resultBottom.getX(), resultBottom.getY(), resultBottom.getWidth(), resultBottom.getHeight() / 1.5f, BodyType.StaticBody, Constants.FIXTURE_BOTTOM)
			.setUserData(Constants.BODY_BOTTOM);
		
		resultBottom.setCullingEnabled(true);
		
		return resultBottom;
	}
	
	protected void createStar(float posX, float posY) {
		createStar(posX, posY, resourceManager.spaceStarRegion);
	}
	
	protected void repeatLvlBackground() {
		if (!backgrounds.isEmpty()) {
			Sprite bgFirst = backgrounds.getFirst();
			Sprite bgSecond = backgrounds.get(1);
			if (bgFirst.getX() + bgFirst.getWidth() < resourceManager.camera.getXMin()) {
				backgrounds.remove();
				backgrounds.add(createBg(bgSecond.getX() + bgSecond.getWidth()));
				bgFirst = null;
			}
		}
		
	}

	protected void repeatBottom() {
		if (!bottoms.isEmpty()) {
			Sprite bottomFirst = bottoms.getFirst();
			Sprite bottomSecond = bottoms.get(1);
			if (bottomFirst.getX() + bottomFirst.getWidth() < resourceManager.camera.getXMin()) {
				bottoms.remove();
				bottoms.add(createBottom(bottomSecond.getX() + bottomSecond.getWidth(), Constants.BOTTOM_Y_POSITION));				
				bottomFirst = null;
			}
		}
	}
	
	protected void addObstacle() {
		ObstacleFactory of = ObstacleFactory.getInstance();
		SpaceObstacle obstacle = of.nextSpaceObstacle();
		obstacles.add(obstacle);
		float posY = (float) Math.random() * 350 + 250;
		createStar(obstacle.getX() + of.getObstacleDistance() / 2, posY);
		
		if (!obstacle.hasParent()) {
			attachChild(obstacle);
		}
	}
	
	@Override
	protected void increaseStarScore() {
		starScore += difficultyMultiplier;
		refreshScoreText();
		sendScoreFlags();
	}
	
	@Override
	public void beginContact(Contact contact) {
		if (Constants.BODY_BOTTOM.equals(contact.getFixtureA().getBody().getUserData()) ||
				Constants.BODY_BOTTOM.equals(contact.getFixtureB().getBody().getUserData())) {
			LevelScene levelScene = new LevelScene(lives.size(), heroXSpeed, score, starScore, state, (int)resourceManager.camera.getHeight() - 100, 0);
			resourceManager.engine.setScene(levelScene);
		}
		if (Constants.BODY_STAR.equals(contact.getFixtureA().getBody().getUserData()) ||
				Constants.BODY_STAR.equals(contact.getFixtureB().getBody().getUserData())) {
			if (starScore % 3 == 0 && starScore > 0) {
				heroXSpeed += 1f;
				ObstacleFactory.getInstance().increaseObstacleDistanceBySpeed(heroXSpeed);
			}	
		}
		super.beginContact(contact);
	}
	
	public void endContact(Contact contact) {
		if (Constants.BODY_SENSOR.equals(contact.getFixtureA().getBody().getUserData()) ||
				Constants.BODY_SENSOR.equals(contact.getFixtureB().getBody().getUserData())) {
			sensorLeaved = true;
		}
		super.endContact(contact);		
	}

	@Override
	protected void startNewGame() {
		// TODO Auto-generated method stub
		
	}
	
}
