package com.testjockey.apps.demo.superjockey.helpers;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.bitmap.BitmapTextureFormat;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;

import android.graphics.Typeface;

import com.testjockey.apps.demo.superjockey.activities.GameActivity;

public class ResourceManager {

	private static final ResourceManager INSTANCE = new ResourceManager();	
	
	public Font font;
	
	public GameActivity activity;
	public Engine engine;
	public Camera camera;
	public VertexBufferObjectManager vbom;
	
	private BitmapTextureAtlas lvl1BottomAtlas;
	private BitmapTextureAtlas lvl2BottomAtlas;
	private BitmapTextureAtlas lvl3BottomAtlas;	
	private BitmapTextureAtlas emptyGroundAtlas;
	
	private BitmapTextureAtlas lvl1bgAtlas;
	private BitmapTextureAtlas lvl2bgAtlas;
	private BitmapTextureAtlas lvl3bgAtlas;
	private BitmapTextureAtlas lvlSpaceBgAtlas;
	
	public TextureRegion lvl1BottomRegion;
	public TextureRegion lvl2BottomRegion;
	public TextureRegion lvl3BottomRegion;
	public TextureRegion emptyGroundRegion;
	
	public TextureRegion lvl1BgRegion;
	public TextureRegion lvl2BgRegion;
	public TextureRegion lvl3BgRegion;
	public TextureRegion lvlSpaceRegion;
	
	private BuildableBitmapTextureAtlas gameObjectsAtlas;	
	
	public TiledTextureRegion suzyRegion;
	public TiledTextureRegion jackRegion;
	
	public TiledTextureRegion starRegion;
	public TiledTextureRegion spaceStarRegion;
	
	public TiledTextureRegion heartRegion;
	
	public TextureRegion candyTopRegion;
	public TextureRegion candyBottomRegion;
	
	public TextureRegion planet1Region;
	public TextureRegion planet2Region;
	public TextureRegion planet3Region;
	public TextureRegion planet4Region;
	
	public Music bgMusic;
	public Sound collectStarSound;
	public Sound dieSound;
	public Sound newLifeSound;
	public Sound lostLifeSound;
	
	public static ResourceManager getInstance() {
		return INSTANCE;
	}
	
	public void create(GameActivity activity, Engine engine, Camera camera, VertexBufferObjectManager vbom) {
		this.activity = activity;
		this.engine = engine;
		this.camera = camera;
		this.vbom = vbom;
	}
	
	
	public void loadGameResources() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		
		lvl1BottomAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 1920, 385, TextureOptions.REPEATING_BILINEAR_PREMULTIPLYALPHA);
		lvl1BottomRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(lvl1BottomAtlas, activity, "level1bottom.png", 0, 0);
		lvl1BottomAtlas.load();
		
		lvl2BottomAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 1920, 376, TextureOptions.REPEATING_BILINEAR_PREMULTIPLYALPHA);
		lvl2BottomRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(lvl2BottomAtlas, activity, "level2bottom.png", 0, 0);
		lvl2BottomAtlas.load();
		
		lvl3BottomAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 1920, 296, TextureOptions.REPEATING_BILINEAR_PREMULTIPLYALPHA);
		lvl3BottomRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(lvl3BottomAtlas, activity, "level3bottom.png", 0, 0);		
		lvl3BottomAtlas.load();
		
		emptyGroundAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 640, 5, TextureOptions.REPEATING_BILINEAR_PREMULTIPLYALPHA);
		emptyGroundRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(emptyGroundAtlas, activity, "emptyground.png", 0, 0);
		emptyGroundAtlas.load();
		
		lvl1bgAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 1920, 1136, TextureOptions.BILINEAR);
		lvl1BgRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(lvl1bgAtlas, activity, "lvl1bg.png",0 ,0);
		lvl1bgAtlas.load();
		
		lvl2bgAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 1920, 1136, TextureOptions.BILINEAR);
		lvl2BgRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(lvl2bgAtlas, activity, "lvl2bg.png",0 ,0);
		lvl2bgAtlas.load();
		
		lvl3bgAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 1920, 1136, TextureOptions.BILINEAR);
		lvl3BgRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(lvl3bgAtlas, activity, "lvl3bg.png",0 ,0);
		lvl3bgAtlas.load();
		
		lvlSpaceBgAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 1920, 1136, TextureOptions.BILINEAR);
		lvlSpaceRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(lvlSpaceBgAtlas, activity, "lvlspacebg.png",0 ,0);
		lvlSpaceBgAtlas.load();
		
		gameObjectsAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 
				2048, 2048, BitmapTextureFormat.RGBA_8888, TextureOptions.REPEATING_BILINEAR_PREMULTIPLYALPHA);
		
		candyTopRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				gameObjectsAtlas, activity.getAssets(), "candytop.png");
		candyBottomRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				gameObjectsAtlas, activity.getAssets(), "candybottom.png");
		
		planet1Region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				gameObjectsAtlas, activity.getAssets(), "lvlspaceplanet1.png");

		planet2Region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				gameObjectsAtlas, activity.getAssets(), "lvlspaceplanet2.png");

		planet3Region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				gameObjectsAtlas, activity.getAssets(), "lvlspaceplanet3.png");

		planet4Region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				gameObjectsAtlas, activity.getAssets(), "lvlspaceplanet4.png");
		
		suzyRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
				gameObjectsAtlas, activity.getAssets(), "suzy.png", 2, 1);
		jackRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
				gameObjectsAtlas, activity.getAssets(), "jack.png", 2, 1);
	
		starRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
				gameObjectsAtlas, activity.getAssets(), "star.png", 4, 1);
		spaceStarRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
				gameObjectsAtlas, activity.getAssets(), "spacestar.png", 4, 1);
		
		heartRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
				gameObjectsAtlas, activity.getAssets(), "heart.png", 2, 1);
		
		try {
			gameObjectsAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(2, 0, 2));
			gameObjectsAtlas.load();
			
		} catch (final TextureAtlasBuilderException e) {
			throw new RuntimeException("Error while loading Splash textures", e);
		}		
		
		try {
			bgMusic = MusicFactory.createMusicFromAsset(activity.getEngine().getMusicManager(), activity, "sfx/sjbg.mp3");
			collectStarSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/collectstar.mp3");
			dieSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/die.mp3");
			newLifeSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/newlife.mp3");
			lostLifeSound = SoundFactory.createSoundFromAsset(activity.getSoundManager(), activity, "sfx/lostlife.mp3");
		} catch (Exception e) {
			throw new RuntimeException("Error while loading sounds", e);
		} 
	}
	
	
	public void loadFont() {
		//Color borderColor = new Color(216/255.f, 0.f, 165/255.f); TJColor
		Color borderColor = new Color(0.f, 0.f, 0.f);
		font = FontFactory.createStroke(activity.getFontManager(), activity.getTextureManager(), 256, 256, Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 70,
				true, Color.WHITE_ABGR_PACKED_INT, 1.5f, borderColor.getARGBPackedInt());
		font.load();
	}

	public void unloadFont() {
		font.unload();
	}
	
}
