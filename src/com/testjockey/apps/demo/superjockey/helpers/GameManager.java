package com.testjockey.apps.demo.superjockey.helpers;

import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.testjockey.apps.demo.superjockey.Constants;
import com.testjockey.apps.demo.superjockey.pojo.Player;

public class GameManager {

	public enum SJDifficulty {
		Easy,
		Medium,
		Hard
	}
	
	private static final GameManager INSTANCE = new GameManager();
	
	private Player player;
	
	private SJDifficulty difficulty = SJDifficulty.Easy;
	private boolean musicOn;
	private float crashFrequency;
	private Vector<Player> bestPlayers = new Vector<Player>();

	public static final GameManager getInstance() {
		return INSTANCE;
	}
	
	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public SJDifficulty getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(SJDifficulty difficulty) {
		this.difficulty = difficulty;
	}

	public boolean isMusicOn() {
		return musicOn;
	}

	public void setMusicOn(boolean musicOn) {
		this.musicOn = musicOn;
	}

	public float getCrashFrequency() {
		return crashFrequency;
	}

	public void setCrashFrequency(float crashFrequency) {
		this.crashFrequency = crashFrequency;
	}
	
	public void setScoreToPlayer(int score, int starScore) {
		this.player.setPoints(score);
		this.player.setStarPoints(starScore);
		addPLayerForBestPLayers(new Player(this.player));
	}

	public Vector<Player> getBestPlayers() {
		return this.bestPlayers;
	}

	public void setBestPlayers(Vector<Player> bestPlayers) {
		this.bestPlayers = bestPlayers;
	}
	
	public int getHighScore() {
		int highScore = 0;
		if (bestPlayers.size() > 0) {
			highScore = this.bestPlayers.firstElement().getOverallPoints();
		}
		return highScore;
	}
	
	private void addPLayerForBestPLayers(Player player) {
		this.bestPlayers.add(player);
		Collections.sort(bestPlayers, Collections.reverseOrder());
		if (bestPlayers.size() > 10) {
			bestPlayers = new Vector<Player>(bestPlayers.subList(0, 9));
		}
	}
	
	public void saveGameManagerDatas(Context context) {
    	SharedPreferences.Editor settingsEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
    	settingsEditor.putFloat(Constants.KEY_CRASH_FERQUENCY, getCrashFrequency());
    	settingsEditor.putInt(Constants.KEY_DIFFICULTY, getDifficulty().ordinal());
    	settingsEditor.putBoolean(Constants.KEY_MUSIC_ON, isMusicOn());
    	Gson gson = new Gson();
    	String playerJson = gson.toJson(getBestPlayers());
    	settingsEditor.putString(Constants.KEY_BESTPLAYERS, playerJson);
    	settingsEditor.commit();
	}
	
	public void fillUpGameManager(Context context) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		setCrashFrequency(sharedPreferences.getFloat(Constants.KEY_CRASH_FERQUENCY, 0.f));
		int difficulty = sharedPreferences.getInt(Constants.KEY_DIFFICULTY, 0);
		switch (difficulty) {
		case 0:
			setDifficulty(SJDifficulty.Easy);
			break;
		case 1:
			setDifficulty(SJDifficulty.Medium);
			break;
		case 2:
			setDifficulty(SJDifficulty.Hard);
			break;
		}
		setMusicOn(sharedPreferences.getBoolean(Constants.KEY_MUSIC_ON, true));
		String playerJson = sharedPreferences.getString(Constants.KEY_BESTPLAYERS, "");
		Gson gson = new Gson();
		Player[] playerArray = gson.fromJson(playerJson, Player[].class);
		if (playerArray != null) {
			Vector<Player> playerVector = new Vector<Player>();
			playerVector.addAll(Arrays.asList(playerArray));
			setBestPlayers(playerVector);			
		}
		String playerName = sharedPreferences.getString(Constants.KEY_PLAYERNAME, "");
		if (!playerName.equals("")) {
			if (player == null) {
				player = new Player();				
			}
			player.setName(playerName);
		}
	}   
	
}
