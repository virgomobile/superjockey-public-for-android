package com.testjockey.apps.demo.superjockey.gameelements;

import org.andengine.entity.IEntity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.testjockey.apps.demo.superjockey.Constants;

public class LevelObstacle extends Obstacle {

	private Sprite candyBottom;
	private Sprite candyTop;
	
	private Body candyBottomBody;
	private Body candyTopBody;
	private Body sensorBody;
	
	public LevelObstacle(float x, float y, int distanceBetweenObstacles, TextureRegion candyTopRegion, TextureRegion candyBottomRegion, VertexBufferObjectManager vbom, PhysicsWorld physics) {
		super(x, y, physics);
		
		candyBottom = new Sprite(0, 0, candyBottomRegion.getWidth(), candyBottomRegion.getHeight(), candyBottomRegion, vbom);
		attachChild(candyBottom);
		
		candyBottomBody = PhysicsFactory.createBoxBody(physics, candyBottom, BodyType.StaticBody, Constants.FIXTURE_OBSTACLE);
		candyBottomBody.setUserData(Constants.BODY_OBSTACLE);

		candyTop = new Sprite(0, candyBottom.getHeight() + distanceBetweenObstacles, candyTopRegion.getWidth(), candyTopRegion.getHeight(), candyTopRegion, vbom);
		attachChild(candyTop);
		
		candyTopBody = PhysicsFactory.createBoxBody(physics, candyTop, BodyType.StaticBody, Constants.FIXTURE_OBSTACLE);
		candyTopBody.setUserData(Constants.BODY_OBSTACLE);

		Rectangle r = new Rectangle(0, 0, 1, 9999, vbom);
		r.setColor(Color.GREEN);
		r.setAlpha(0f);
		attachChild(r);
		
		sensorBody = PhysicsFactory.createBoxBody(physics, r, BodyType.StaticBody, Constants.FIXTURE_SENSOR);
		sensorBody.setUserData(Constants.BODY_SENSOR);
	}
	
	@Override
	public boolean collidesWith(IEntity pOtherEntity) {
		return candyBottom.collidesWith(pOtherEntity) || candyTop.collidesWith(pOtherEntity);
	}
	
	public void setInactive() {
		candyTopBody.setActive(false);
		candyBottomBody.setActive(false);
	}

	public void destroy() {
		detachChild(candyTop);
		detachChild(candyBottom);
		setInactive();
		physics.destroyBody(candyTopBody);
		physics.destroyBody(candyBottomBody);
		physics.destroyBody(sensorBody);
		candyTop = null;
		candyTopBody = null;
		candyBottom = null;
		candyBottomBody = null;
	}

	@Override
	public float getWidth() {
		return candyTop.getWidth();
	}
	
}
