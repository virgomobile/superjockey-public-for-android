package com.testjockey.apps.demo.superjockey.gameelements;

import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.extension.physics.box2d.PhysicsWorld;

public abstract class Obstacle extends Entity {

	protected PhysicsWorld physics;
	
	public Obstacle(float x, float y, PhysicsWorld physics) {
		super(x, y);
		this.physics = physics;
	}

	abstract public boolean collidesWith(IEntity pOtherEntity);
	
	abstract public void setInactive();

	abstract public void destroy();
	
	abstract public float getWidth();
	
}
