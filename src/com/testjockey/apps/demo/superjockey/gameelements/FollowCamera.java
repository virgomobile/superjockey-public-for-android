package com.testjockey.apps.demo.superjockey.gameelements;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.IEntity;
import org.andengine.util.Constants;

public class FollowCamera extends Camera {

	IEntity ieChase;
	
	private float camWidth;
	
	public FollowCamera(float pX, float pY, float pWidth, float pHeight) {
		super(pX, pY, pWidth, pHeight);
		camWidth = pWidth;
	}
	
	public void setChaseEntity(final IEntity pChaseEntity) {
		this.ieChase = pChaseEntity;
	}

	@Override
	public void updateChaseEntity() {
		if (this.ieChase != null) {
			final float[] centerCoordinates = this.ieChase.getSceneCenterCoordinates();
			float camx = centerCoordinates[Constants.VERTEX_INDEX_X];			
			this.setCenter(camx + ((camWidth / 2) - com.testjockey.apps.demo.superjockey.Constants.HERO_X_POS), this.getCenterY());
		}
	}

}
