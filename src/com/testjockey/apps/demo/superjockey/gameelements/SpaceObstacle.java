package com.testjockey.apps.demo.superjockey.gameelements;

import org.andengine.entity.IEntity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.testjockey.apps.demo.superjockey.Constants;
import com.testjockey.apps.demo.superjockey.helpers.ResourceManager;

public class SpaceObstacle extends Obstacle {

	private Sprite planet;
	
	private Body planetBody;
	private Body sensorBody;
	
	public SpaceObstacle(float x, float y, VertexBufferObjectManager vbom, PhysicsWorld physics) {
		super(x, y, physics);

		int planetNumber = (int)(Math.random() * 4);
		switch (planetNumber) {
            case 0:
                planet = new Sprite(0, y + Constants.CH / 2, ResourceManager.getInstance().planet1Region, vbom);
                break;
            case 1:
                planet = new Sprite(0, y + Constants.CH / 2, ResourceManager.getInstance().planet2Region, vbom);
                break;
            case 2:
                planet = new Sprite(0, y + Constants.CH / 2, ResourceManager.getInstance().planet3Region, vbom);
                break;
            case 3:
                planet = new Sprite(0, y + Constants.CH / 2, ResourceManager.getInstance().planet4Region, vbom);
                break;
        }
		
		attachChild(planet);

		planetBody = PhysicsFactory.createCircleBody(physics, planet, BodyType.StaticBody, Constants.FIXTURE_OBSTACLE);
		planetBody.setUserData(Constants.BODY_OBSTACLE);

		Rectangle r = new Rectangle(0, 0, 1, 9999, vbom);
		r.setColor(Color.GREEN);
		r.setAlpha(0f);
		attachChild(r);
		
		sensorBody = PhysicsFactory.createBoxBody(physics, r, BodyType.StaticBody, Constants.FIXTURE_SENSOR);
		sensorBody.setUserData(Constants.BODY_SENSOR);
	}
	
	@Override
	public boolean collidesWith(IEntity pOtherEntity) {
		return planet.collidesWith(pOtherEntity);
	}
	
	public void setInactive() {
		planetBody.setActive(false);
	}
	
	public void destroy() {
		detachChild(planet);
		setInactive();
		physics.destroyBody(planetBody);
		planet = null;
		planetBody = null;
	}

	@Override
	public float getWidth() {
		return planet.getWidth();
	}
	
}
