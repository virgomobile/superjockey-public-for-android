package com.testjockey.apps.demo.superjockey.pojo;

import java.io.Serializable;

public class Player implements Serializable, Comparable<Player> {

	private static final long serialVersionUID = 1L;

	public enum SJCharacterName {
		SJSuzy,
		SJack
	}
	
	private String name;
	private int points;
	private int starPoints;
	private SJCharacterName selectedCharacter;
	
	public Player() {
	}
	
	public Player(Player player) {
		this.name = player.name;
		this.points = player.points;
		this.starPoints = player.starPoints;
		this.selectedCharacter = player.selectedCharacter;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public int getStarPoints() {
		return starPoints;
	}

	public void setStarPoints(int starPoints) {
		this.starPoints = starPoints;
	}

	public SJCharacterName getSelectedCharacter() {
		return selectedCharacter;
	}

	public void setSelectedCharacter(SJCharacterName selectedCharacter) {
		this.selectedCharacter = selectedCharacter;
	}
	
	public Integer getOverallPoints() {
		return Integer.valueOf(this.points + this.starPoints);
	}

	@Override
	public int compareTo(Player anotherPlayer) {
		return getOverallPoints().compareTo(anotherPlayer.getOverallPoints());
	}
	
}
