package com.testjockey.apps.demo.superjockey.activities;

import com.testjockey.apps.demo.superjockey.helpers.GameManager;
import com.testjockey.apps.demo.superjockey.helpers.GameManager.SJDifficulty;
import com.testjockey.apps.demo.superjockey.Constants;
import com.testjockey.apps.demo.superjockey.R;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.ToggleButton;

public class SettingsActivity extends Activity {

	private Button btDifficultyEasy, btDifficultyMedium, btDifficultyHard;
	private ToggleButton tbMusic;
	private SeekBar sbCrashFrequency;
	private GameManager gameManager = GameManager.getInstance();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        
        btDifficultyEasy = (Button)findViewById(R.id.btEasy);
        btDifficultyMedium = (Button)findViewById(R.id.btMedium);
        btDifficultyHard = (Button)findViewById(R.id.btHard);
        tbMusic = (ToggleButton)findViewById(R.id.tbMUsicOnOff);
        sbCrashFrequency = (SeekBar)findViewById(R.id.sbCrashFrequency);
        
        btDifficultyEasy.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				gameManager.setDifficulty(SJDifficulty.Easy);
				checkSelectedDifficultyButton();
				save();				
			}
		});
        
        btDifficultyMedium.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				gameManager.setDifficulty(SJDifficulty.Medium);
				checkSelectedDifficultyButton();
				save();				
			}
		});
        
        btDifficultyHard.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				gameManager.setDifficulty(SJDifficulty.Hard);
				checkSelectedDifficultyButton();
				save();				
			}
		});
        
        tbMusic.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				gameManager.setMusicOn(isChecked);
				save();				
			}
		});
        
        sbCrashFrequency.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				gameManager.setCrashFrequency(((float)progress / 100));
				save();
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
        tbMusic.setChecked(gameManager.isMusicOn());
        sbCrashFrequency.setProgress((int)(gameManager.getCrashFrequency() * 100));
        checkSelectedDifficultyButton();
	}
	
	private void checkSelectedDifficultyButton() {
		switch (gameManager.getDifficulty()) {
		case Easy:
			btDifficultyEasy.setBackgroundColor(getResources().getColor(R.color.tjpurple));
			btDifficultyMedium.setBackgroundColor(android.graphics.Color.WHITE);
			btDifficultyHard.setBackgroundColor(android.graphics.Color.WHITE);
			btDifficultyEasy.setTextColor(android.graphics.Color.WHITE);
			btDifficultyMedium.setTextColor(getResources().getColor(R.color.tjpurple));
			btDifficultyHard.setTextColor(getResources().getColor(R.color.tjpurple));
			break;
		case Medium:
			btDifficultyEasy.setBackgroundColor(android.graphics.Color.WHITE);
			btDifficultyMedium.setBackgroundColor(getResources().getColor(R.color.tjpurple));
			btDifficultyHard.setBackgroundColor(android.graphics.Color.WHITE);
			btDifficultyEasy.setTextColor(getResources().getColor(R.color.tjpurple));
			btDifficultyMedium.setTextColor(android.graphics.Color.WHITE);
			btDifficultyHard.setTextColor(getResources().getColor(R.color.tjpurple));
			break;
		case Hard:
			btDifficultyEasy.setBackgroundColor(android.graphics.Color.WHITE);
			btDifficultyMedium.setBackgroundColor(android.graphics.Color.WHITE);
			btDifficultyHard.setBackgroundColor(getResources().getColor(R.color.tjpurple));
			btDifficultyEasy.setTextColor(getResources().getColor(R.color.tjpurple));
			btDifficultyMedium.setTextColor(getResources().getColor(R.color.tjpurple));
			btDifficultyHard.setTextColor(android.graphics.Color.WHITE);
			break;
        }
	}
	
	public void save() {
		SharedPreferences.Editor settingsEditor = PreferenceManager.getDefaultSharedPreferences(this).edit();
    	settingsEditor.putFloat(Constants.KEY_CRASH_FERQUENCY, gameManager.getCrashFrequency());
    	settingsEditor.putInt(Constants.KEY_DIFFICULTY, gameManager.getDifficulty().ordinal());
    	settingsEditor.putBoolean(Constants.KEY_MUSIC_ON, gameManager.isMusicOn());
    	settingsEditor.commit();
	}
	
	@Override
	public void onBackPressed() {
		save();
		finish();
	}
	
}
