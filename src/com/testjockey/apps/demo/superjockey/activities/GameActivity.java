package com.testjockey.apps.demo.superjockey.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;

import com.google.gson.Gson;
import com.testjockey.apps.demo.superjockey.Constants;
import com.testjockey.apps.demo.superjockey.gameelements.FollowCamera;
import com.testjockey.apps.demo.superjockey.helpers.GameManager;
import com.testjockey.apps.demo.superjockey.helpers.ResourceManager;
import com.testjockey.apps.demo.superjockey.scenes.LevelScene;

import java.io.IOException;

import org.andengine.engine.Engine;
import org.andengine.engine.LimitedFPSEngine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.CropResolutionPolicy;
import org.andengine.engine.options.resolutionpolicy.IResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.view.RenderSurfaceView;
import org.andengine.ui.activity.BaseGameActivity;

public class GameActivity extends BaseGameActivity {

	private Camera camera;
	
	private LevelScene levelScene;
	
	private SharedPreferences prefs;
	
	private GameManager gameManager = GameManager.getInstance();
	
	public RenderSurfaceView surfaceView;
	
	@Override
	public Engine onCreateEngine(EngineOptions pEngineOptions) {
		Engine engine = new LimitedFPSEngine(pEngineOptions, Constants.FPS_LIMIT);
		return engine;
	}

	@Override
	public EngineOptions onCreateEngineOptions() {
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int resHeight = 1136;
		int resWidth = (int)(resHeight * ((float)dm.widthPixels / (float)dm.heightPixels));
		
		prefs = PreferenceManager.getDefaultSharedPreferences(this);		
		camera = new FollowCamera(0, 0, resWidth, resHeight);
		IResolutionPolicy resolutionPolicy = new CropResolutionPolicy(resWidth, resHeight);
		EngineOptions engineOptions = 
				new EngineOptions(true, ScreenOrientation.PORTRAIT_FIXED, resolutionPolicy, camera);
		engineOptions.getAudioOptions().setNeedsMusic(true).setNeedsSound(true);
		engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
		return engineOptions;
	}

	@Override
	public void onCreateResources(
			OnCreateResourcesCallback pOnCreateResourcesCallback)
			throws IOException {
		ResourceManager resourceManager = ResourceManager.getInstance();
		resourceManager.create(this, getEngine(), camera, getVertexBufferObjectManager());
		ResourceManager.getInstance().loadFont();		
		resourceManager.loadGameResources();
		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
			throws IOException {
		levelScene = new LevelScene();
		surfaceView = mRenderSurfaceView;
		handleMusic();
		pOnCreateSceneCallback.onCreateSceneFinished(levelScene);
	}

	@Override
	public void onPopulateScene(Scene pScene,
			OnPopulateSceneCallback pOnPopulateSceneCallback)
			throws IOException {
		pScene.reset();
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}
	
	public void save() {
		SharedPreferences.Editor settingsEditor = prefs.edit();
    	settingsEditor.putFloat(Constants.KEY_CRASH_FERQUENCY, gameManager.getCrashFrequency());
    	settingsEditor.putInt(Constants.KEY_DIFFICULTY, gameManager.getDifficulty().ordinal());
    	settingsEditor.putBoolean(Constants.KEY_MUSIC_ON, gameManager.isMusicOn());
    	Gson gson = new Gson();
    	String playersJson = gson.toJson(gameManager.getBestPlayers());
    	settingsEditor.putString(Constants.KEY_BESTPLAYERS, playersJson);
    	settingsEditor.putString(Constants.KEY_PLAYERNAME, gameManager.getPlayer().getName());
    	settingsEditor.commit();
	}
	
	@Override
	public synchronized void onResumeGame() {
		super.onResumeGame();
		levelScene.resume();
		handleMusic();
	}

	private void handleMusic() {
		ResourceManager resourceManager = ResourceManager.getInstance();
		if (gameManager.isMusicOn()) {
			resourceManager.bgMusic.play();
			resourceManager.bgMusic.setLooping(true);
		} else {
			resourceManager.bgMusic.stop();
		}
	}

	@Override
	public synchronized void onPauseGame() {
		super.onPauseGame();
		levelScene.pause();
	}

	@Override
	public void onBackPressed() {
		startActivity(new Intent(GameActivity.this, MenuActivity.class));
		finish();
	}

	
	
}
