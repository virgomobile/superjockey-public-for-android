package com.testjockey.apps.demo.superjockey.activities;

import com.testjockey.apps.demo.superjockey.R;
import com.testjockey.apps.demo.superjockey.helpers.GameManager;
import com.testjockey.apps.demo.superjockey.helpers.PlayerListAdapter;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

public class ScoresActivity extends Activity {

	private ListView lvScores;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);
        
        lvScores = (ListView)findViewById(R.id.lvScores);
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		lvScores.setAdapter(new PlayerListAdapter(this, R.layout.row_highscore, GameManager.getInstance().getBestPlayers()));
	}

	@Override
	public void onBackPressed() {
		finish();
	}
}
