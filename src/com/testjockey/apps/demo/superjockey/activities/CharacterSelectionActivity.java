package com.testjockey.apps.demo.superjockey.activities;

import hu.virgo.testjockeysdk.PlayCallback;
import hu.virgo.testjockeysdk.core.APIError;
import hu.virgo.testjockeysdk.core.TestJockey;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import com.testjockey.apps.demo.superjockey.Constants;
import com.testjockey.apps.demo.superjockey.helpers.GameManager;
import com.testjockey.apps.demo.superjockey.pojo.Player;
import com.testjockey.apps.demo.superjockey.pojo.Player.SJCharacterName;
import com.testjockey.apps.demo.superjockey.R;

public class CharacterSelectionActivity extends Activity {
	
	private SharedPreferences sharedPreferences;
	
	private ImageButton ibSuzy, ibJack;
	private GameManager gameManager = GameManager.getInstance();
	private Player player = new Player();

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_characterselection);
                
        if (gameManager.getPlayer() == null) {
            gameManager.fillUpGameManager(this);
        }
        
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);        
        
        String apiKey = null;

        //TJ SDK-s indításhoz
        if(getIntent() != null) {

            if(getIntent().getData() != null) {
                //ezt kéne arra felhasználni, hogy a TJ SDK innen vegye az apikey-t
               apiKey = getIntent().getData().getQueryParameter("apikey");
            }
        }
		
        if (apiKey == null) {
        	if (readApiKey() != null) {
        		apiKey = readApiKey();
        	}
        } else {
        	saveApiKey(apiKey);
        }
        
        if (apiKey != null) {
            TestJockey.play(new PlayCallback() {
    			
    			@Override
    			public void onWarning(String arg0) {
    			}
    			
    			@Override
    			public void onSuccess() {
    			}
    			
    			@Override
    			public boolean onFail(APIError arg0) {
    				return false;
    			}
    		}, this, apiKey, false);
        } else {
        	AlertDialog.Builder dlgExit = new AlertDialog.Builder(this);
            dlgExit.setTitle(getResources().getString(R.string.without_tj_title));
            dlgExit.setMessage(getResources().getString(R.string.without_tj_description));
            dlgExit.setCancelable(false);
            dlgExit.setPositiveButton(getResources().getString(R.string.okay), new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					System.exit(0);
				}
			});
            dlgExit.create();
            dlgExit.show();
        }
        
        ibSuzy = (ImageButton)findViewById(R.id.ibSuzy);
        ibSuzy.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				TestJockey.pinFlag(TestJockey.Flag.builder(Constants.TJ_FLAG_SUZY_SELECTED).requestTelemetry(true).build());
				player.setSelectedCharacter(SJCharacterName.SJSuzy);
				if (isBaseNameNeedToSet()) {
					player.setName(getResources().getString(R.string.suzy_name));
				}
				startActivity(new Intent(CharacterSelectionActivity.this, MenuActivity.class));
				finish();
			}
		});
        
        ibJack = (ImageButton)findViewById(R.id.ibJack);
        ibJack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				TestJockey.pinFlag(TestJockey.Flag.builder(Constants.TJ_FLAG_JACK_SELECTED).requestTelemetry(true).build());				
				player.setSelectedCharacter(SJCharacterName.SJack);
				if (isBaseNameNeedToSet()) {
					player.setName(getResources().getString(R.string.jack_name));					
				}
				startActivity(new Intent(CharacterSelectionActivity.this, MenuActivity.class));
				finish();				
			}
		});
        
        gameManager.setPlayer(player);
    }

	@Override
    protected void onStop() {
    	gameManager.saveGameManagerDatas(this);
    	super.onStop();
    } 
	
	private void saveApiKey(String apiKey) {
		SharedPreferences.Editor settingsEditor = sharedPreferences.edit();
		settingsEditor.putString(Constants.KEY_APIKEY, apiKey);
    	settingsEditor.commit();
	}
	
	private String readApiKey() {
		return sharedPreferences.getString(Constants.KEY_APIKEY, null);
	}
	
	private boolean isBaseNameNeedToSet() {
		boolean result = false;
		if (gameManager.getPlayer().getName() == null ||
				gameManager.getPlayer().getName().equals(getResources().getString(R.string.jack_name)) ||
				gameManager.getPlayer().getName().equals(getResources().getString(R.string.suzy_name)) ||
				gameManager.getPlayer().getName().equals("")) {
			result = true;
		}
		return result;
	}
    
}
