package com.testjockey.apps.demo.superjockey;

import org.andengine.extension.physics.box2d.PhysicsFactory;

import com.badlogic.gdx.physics.box2d.FixtureDef;

public class Constants {

	public static final int FPS_LIMIT = 60;
	public static final int CW = 540;
	public static final int CH = 960;

	public final static String BODY_OBSTACLE = "obstacle";
	public final static String BODY_SENSOR = "sensor";
	public final static String BODY_HERO = "hero";
	public final static String BODY_BACKGROUND = "background";
	public final static String BODY_STAR = "star";
	public final static String BODY_TOP = "top";
	public final static String BODY_BOTTOM = "bottom";
	
	public static final FixtureDef FIXTURE_OBSTACLE = PhysicsFactory.createFixtureDef(1f, 0f, 1f, true);
	public static final FixtureDef FIXTURE_LEVEL = PhysicsFactory.createFixtureDef(1f, 0f, 1f, false);
	public static final FixtureDef FIXTURE_SENSOR = PhysicsFactory.createFixtureDef(1f, 0f, 1f, true);
	public static final FixtureDef FIXTURE_HERO = PhysicsFactory.createFixtureDef(1f, 0f, 1f, false);
	public static final FixtureDef FIXTURE_TOP = PhysicsFactory.createFixtureDef(1f, 0f, 1f, false);
	public static final FixtureDef FIXTURE_BOTTOM = PhysicsFactory.createFixtureDef(1f, 0f, 1f, false);
	public static final FixtureDef FIXTURE_BACKGROUND = PhysicsFactory.createFixtureDef(1f, 0f, 1f, true);
	public static final FixtureDef FIXTURE_STAR = PhysicsFactory.createFixtureDef(1f, 0f, 1f, true);
	
	public static final float GRAVITY = -45;
	public static final float SPEED_X = 6;
	public static final float SPEED_Y = 17;
	
	public static final float HERO_X_POS = 200;
	
	public static final float DISTANCE_BETWEEN_OBSTACLES = 300.f;
	public static final int PIPE_DISTANCE = 330;
	
	public static final String KEY_HISCORE = "hiscore";
	public static final String KEY_CRASH_FERQUENCY = "crashfrequency";
	public static final String KEY_DIFFICULTY = "difficulty";
	public static final String KEY_MUSIC_ON = "musicon";
	public static final String KEY_BESTPLAYERS = "bestplayers";
	public static final String KEY_PLAYERNAME = "playername";
	
	public static final String KEY_APIKEY = "apikey";
	
	public static final boolean DEBUG_MODE = false;
	public static final float BOTTOM_Y_POSITION = -100.f;
	public static final int STAR_MULTIPLIER = 5;
	
	public static final int TJ_SCORE_TO_PIN = 50;	
	public static final long TIME_TO_RESSURECTION = 200;
	
	public static final String TJ_FLAG_POINT_REACHED = " point reached";
	public static final String TJ_FLAG_GAME_OVER = "Game over";
	public static final String TJ_FLAG_HISCORE = "New highscore reached";
	public static final String TJ_FLAG_STAR_COLLECTED = "Star collected";
	public static final String TJ_FLAG_LEVEL2 = "Level 2 reached";
	public static final String TJ_FLAG_LEVEL3 = "Level 3 reached";
	public static final String TJ_FLAG_LEVEL_SPACE = "Space level reached";
	public static final String TJ_FLAG_SUZY_SELECTED = "TJSuzy selected";
	public static final String TJ_FLAG_JACK_SELECTED = "TJJack selected";
	
}
