package superjockey;

public class TJThereIsNoFateButWhatWeMakeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public TJThereIsNoFateButWhatWeMakeException() {
		super("TJThereIsNoFateButWhatWeMakeException");
	}

}
